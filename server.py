#!/usr/bin/python3
import bottle
import os
import argparse

app = application = bottle.default_app()

def set_brightness(value):
    set_value_to_file(value, '/sys/class/backlight/rpi_backlight/brightness')

def set_backlight(value):
    set_value_to_file(value, '/sys/class/backlight/rpi_backlight/bl_power')

def set_value_to_file(value, file_path):
    new_value = str(value)
    with open(file_path, 'r') as f:
        current_value = f.read()
        if current_value.strip() == new_value.strip():
            return
    with open(file_path, 'w') as f:
        f.write(new_value)

@bottle.route('/command/<name>')
def index(name):
	return index_with_value(name, None)
	  
@bottle.route('/command/<name>/<value>')
def index_with_value(name, value):
    result = 'unrecognized command'
    command = name
    if command == "screensaver-on":
        os.popen("xscreensaver-command -activate")
        result = 'Screensaver activated!'
    elif command == "screensaver-off":
        set_backlight(0) # set backlight on to see the screen
        os.popen("xscreensaver-command -deactivate")
        result = 'Screensaver deactivated!'
    elif command == "backlight-on":
        set_backlight(0)
        result = 'Backlight activated!'
    elif command == "backlight-off":
        set_backlight(1)
        result = 'Backlight deactivated!'
    elif command == "set-brightness":
        try:
            int_value = int(value)
            if int_value > 255 or int_value < 0:
                result = 'Brightness set failed: ' + str(int_value)
            else:
                if int_value <= 12 and int_value > 0:
                    int_value = 12
                set_brightness(int_value)
                result = 'Brightness set: ' + str(int_value)
        except ValueError:
	        result = 'Brightness set failed:' + str(value)
    return result

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-port", default=8081, type=int)
    args = parser.parse_args()
    bottle.run(host = '0.0.0.0', port = args.port)
